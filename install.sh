#!/bin/bash
echo "Installed Bash Profile.";
rm -r ~/.bash_aliases ~/.bashrc ~/.gitconfig ~/.profile 2> /dev/null;
ln -s ~/.dotfiles/home/.[^.]* ~/; # configs
rm -r ~/.bash 2> /dev/null;
ln -s ~/.dotfiles/.bash ~/.bash; # bash functions
touch ~/.environment;
echo "Installed Bin Files.";
rm -r ~/.bin 2> /dev/null;
ln -s ~/.dotfiles/bin ~/bin;
#curl https://gist.githubusercontent.com/nl5887/a511f172d3fb3cd0e42d/raw/d2f8a07aca44aa612b6844d8d5e53a05f5da3420/transfer.sh > ~/bin/transfer.sh; chmod +x ~/bin/transfer.sh;
#ln -s ~/.dotfiles/bin/transfer.sh ~/bin/transfer.sh;
echo "Please logout and log back in to apply environmental scripts or source .bash_profile.";
exit 0;
