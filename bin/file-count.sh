#!/bin/bash
du -a "$1" | cut -d/ -f2 | sort | uniq -c | sort -n
exit
