#!/bin/bash
FILEDATE="$(date +"%Y%m%d_%H%M%S")"
CONF="root@10.10.0.1:/cf/conf/config.xml"
DEST="/home/michael/pfsense"
scp $CONF $DEST/config-$FILEDATE.xml
exit
