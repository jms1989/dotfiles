#!/bin/bash
rm -r ~/Documents ~/Downloads ~/Music ~/Pictures/ ~/Videos
ln -s /media/2TB_Storage/Documents ~/Documents
ln -s /media/2TB_Storage/Downloads ~/Downloads
ln -s /media/2TB_Storage/Pictures ~/Pictures
ln -s /media/2TB_Storage/Music ~/Music
ln -s /media/2TB_Storage/Videos ~/Videos
ln -s /media/SSD_Storage/BTSync ~/BTSync
ln -s /media/2TB_Storage/isos ~/isos
ln -s /home/michael/Calibre\ Library ~/Calibre\ Library
ln -s /home/michael/bin ~/bin
ln -s /home/michael/Games ~/Games
ln -s /home/michael/vbox_vms ~/vbox_vms
ln -s /home/michael/docker ~/docker
ln -s /home/michael/docker-volumes ~/docker-volumes
ln -s /home/michael/Dropbox ~/Dropbox
ln -s /home/michael/.sync ~/.sync
ln -s /home/michael/.dotfiles ~/.dotfiles
ln -s /home/michael/.putty ~/.putty
ln -s /home/michael/.ssh ~/.ssh
ln -s /home/michael/terminator.ppk ~/terminator.ppk
ln -s /home/michael/.config/filezilla ~/.config/filezilla
ln -s /home/michael/.config/hexchat ~/.config/hexchat
ln -s /home/michael/.local/share/American\ Truck\ Simulator ~/.local/share/American\ Truck\ Simulator
ln -s /home/michael/.local/share/Euro\ Truck\ Simulator\ 2 ~/.local/share/Euro\ Truck\ Simulator\ 2
ln -s /home/michael/.local/share/TelegramDesktop ~/.local/share/TelegramDesktop
exit