#!/bin/bash

for i in ./*; do
    d=${i%/}
    dl=${d,,}
    [[ $d = $dl ]] && continue
    if [[ -e $dl ]]; then
        if [[ ! -L $dl ]]; then
            echo "    ERROR: $dl is not a link"
        elif [[ $(readlink -- "$dl") != $d ]]; then
            echo "    ERROR $dl exists and doesn't point to $d"
        fi
    else
        ln -sv -- "$d" "$dl"
    fi
done
