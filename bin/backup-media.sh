#!/bin/bash
# A script to backup the media server

#DRIVE_POWER_URL="http://slavepigpio1.tailc5061.ts.net:1880/server/drives/backups"
#DRIVE_POWER_URL="http://backup-hdd-pwr-590744-1860.sanlan/cm?cmnd=Power1"
#bash command "curl -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI0NTg3YzE4ZDliZTg0Zjc0YjYxMTBkY2Y5ZWJmNTc1OCIsImlhdCI6MTcyMDM3NjUyNCwiZXhwIjoyMDM1NzM2NTI0fQ.PCfPHw347tjs58O0RY1CbKQ1nyNrvL4yGRpmmMuJu08" -H "Content-Type: application/json" -d '{"entity_id": "switch.external_hdd_array_external_hdd_power"}' http://homeassistant:8123/api/services/switch"
HA_AUTH="Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI0NTg3YzE4ZDliZTg0Zjc0YjYxMTBkY2Y5ZWJmNTc1OCIsImlhdCI6MTcyMDM3NjUyNCwiZXhwIjoyMDM1NzM2NTI0fQ.PCfPHw347tjs58O0RY1CbKQ1nyNrvL4yGRpmmMuJu08"
HA_ENTITY=switch.external_hdd_array_external_hdd_power
HA_URL="http://homeassistant:8123/api/services/switch"

function toggle_power {
    #curl $DRIVE_POWER_URL
    #curl $DRIVE_POWER_URL%20TOGGLE;
    #$DRIVE_POWER_URL/toggle;
    curl --header "$HA_AUTH" --header "Content-Type: application/json" -d "{\"entity_id\": \"$HA_ENTITY\"}"  $HA_URL/toggle;
}

function power_on {
    #curl $DRIVE_POWER_URL/on
    #curl $DRIVE_POWER_URL%20ON;
    #$DRIVE_POWER_URL/turn_on;
    curl --header "$HA_AUTH" --header "Content-Type: application/json" -d "{\"entity_id\": \"$HA_ENTITY\"}" $HA_URL/turn_on;
}

function power_off {
    #curl $DRIVE_POWER_URL/off
    #curl $DRIVE_POWER_URL%20OFF;
    #$DRIVE_POWER_URL/turn_off;
    curl --header "$HA_AUTH" --header "Content-Type: application/json" -d "{\"entity_id\": \"$HA_ENTITY\"}" $HA_URL/turn_off;
}

function mount_disks {
    sudo mount --label backup /mnt/backup-disk0
    sudo mount --label backup2 /mnt/backup-disk1
    sudo mount --label backup3 /mnt/backup-disk2
    sudo mount --label backup4 /mnt/backup-disk3
    sudo mount --label backup5 /mnt/backup-disk4
}

function unmount_disks {
    sudo umount /mnt/backup /mnt/backup-disk*
}

function mergerfs {
    sudo mergerfs -o allow_other,use_ino,cache.files=off,category.create=mfs /mnt/backup-disk0:/mnt/backup-disk1:/mnt/backup-disk2:/mnt/backup-disk3:/mnt/backup-disk4 /mnt/backup
}

function backup {
    rsync -avP --backup --ignore-missing-args ‐‐ignore‐existing --log-file=backup.log --exclude=.Trash-1000 --exclude=Downloads --exclude=tmp /media/drives/ /mnt/backup/
}

if [ "$1" == "on" ]; then
    power_on
elif [ "$1" == "off" ]; then
    power_off
elif [ "$1" == "toggle" ]; then
    toggle_power
elif [ "$1" == "mount" ]; then
    mount_disks
    mergerfs
elif [ "$1" == "unmount" ]; then    
    unmount_disks
elif [ "$1" == "backup" ]; then
    backup
elif [ "$1" == "help" ]; then
    echo "Usage: backup-media.sh [on|off|toggle|mount|unmount|backup|help]"
else
# Main
power_on; sleep 30;
mount_disks; mergerfs; sleep 5;
backup; sleep 5;
unmount_disks; sleep 5;
power_off
fi
