#!/bin/bash
rsync --progress --bwlimit=150000 --remove-source-files --partial --prune-empty-dirs -rv --exclude=.sync jms1989@whatbox:dl/tv-sonarr/ /media/drives/Downloads/TV/
echo
#echo "move files from temp to downloads"
#mv -v /media/drives/tmp/tv/* /media/drives/Downloads/TV/
#echo
echo "delete empty dirs"
find /media/drives/Downloads/TV/ -type f -name "{*.png,*.txt}" -delete -print
find /media/drives/Downloads/TV/ -type d -empty -delete -print
echo
ssh jms1989@whatbox "find dl/tv-sonarr/ -mindepth 1 -maxdepth 1 -type d -empty -delete"
exit
