#!/bin/bash

# Define the prefix you want to filter (e.g., OpenStack-Ansible/All-in-One uses "aio" as a prefix for every of its LXCs)
container_prefix=""

LXC_CFG="/var/lib/lxc/${container_prefix}*"

for IFNAME in $(grep "lxc\.network\.veth\.pair = " ${LXC_CFG}/config -R | awk 'match($0, /b(:?.*):lxc\.network\.veth\.pair = (.*)/) {print $3}'); do
  echo "$IFNAME"
  ip link delete $IFNAME
done
