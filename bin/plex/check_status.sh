#!/bin/bash
STATUS="$(systemctl is-active plexmediaserver.service)"
if systemctl is-active --quiet plexmediaserver.service; then
    echo "Execute your tasks ....."
else 
    echo " Service not running.... so exiting "  
    exit 1  
fi