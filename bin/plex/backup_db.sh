#!/bin/bash
[ "$UID" -eq 0 ] || exec sudo bash "$0" "$@"

TIME=`date +%Y%m%d`
FILENAME=plexdb_$TIME.tgz
BASEDIR="/var/lib/plexmediaserver"
SRCDIR="Library/Application Support/Plex Media Server/Plug-in Support/Databases"
DESDIR="/home/michael/backup"

backup () {
    cd $BASEDIR && \
    tar -pczf $DESDIR/$FILENAME "$SRCDIR" && \
    chown michael:michael $DESDIR/$FILENAME && \
    echo "backup complete" && \
    systemctl start plexmediaserver.service && \
    echo "plex is now running"
}

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
else
    if systemctl is-active --quiet plexmediaserver.service; then
        systemctl stop plexmediaserver.service && \
        backup
    else 
        backup
        exit 1  
    fi
fi
exit