#!/bin/bash
rsync --remove-source-files --append-verify --inplace --prune-empty-dirs -Prv --exclude=.sync jms1989@whatbox:dl/radarr/ /media/drives/tmp/movies/
echo
echo "move files from temp to downloads"
mv /media/drives/tmp/movies/* /media/drives/Downloads/Movies/
echo
echo "delete empty dirs"
find /media/drives/Downloads/Movies/ -type d -empty -delete -print
echo
ssh jms1989@whatbox "find ~/dl/radarr/  -type d -empty -delete"
exit

