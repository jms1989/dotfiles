alias df='df -h -x squashfs -x overlay -x shm'
#alias ytdl='youtube-dl -x -k --audio-format mp3 -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4 --write-all-thumbnails --embed-thumbnail -i --download-archive completed.txt'
alias upgrade='sudo apt update && sudo apt upgrade -y'

ytdl () {
	youtube-dl -x -k --audio-format mp3 -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4 --write-all-thumbnails --embed-thumbnail -i --download-archive completed.txt --console-title --add-metadata $1;
	rm -f *.f*;
}
ytdl-video () {
	youtube-dl -f bestvideo+bestaudio[ext=webm] --console-title --add-metadata $1;
}

alias onedrive="onedrive --synchronize"

alias compose="nano docker-compose.yml"
alias compose-up="docker compose up -d"
alias compose-down="docker compose down"
alias compose-restart="docker compose restart"
alias status="docker compose ps"
# server aliases
alias whatbox="ssh jms1989@whatbox"
alias fileserver="ssh fileserver"
alias pfsense="ssh root@pfsense"
alias webdev="ssh michael@webdev"

# Source some bash functions
#source "${HOME}/.bash/kill_space_engineers.bash"
#source "${HOME}/.bash/digitalocean_completion.bash"
#source "${HOME}/.bash/k8s_completion.bash"

alias myip="dig +short myip.opendns.com @resolver1.opendns.com"

alias bind="cd services/bind9/config"
