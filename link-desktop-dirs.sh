#!/bin/bash
echo "Linking desktop dirs."
ln -vs /media/SSD_Storage/BTSync ~/;
ln -vs /media/2TB_Storage/Documents ~/;
ln -vs /media/2TB_Storage/Downloads
ln -vs /media/2TB_Storage/isos ~/;
ln -vs /media/2TB_Storage/Music ~/;
ln -vs /media/2TB_Storage/Pictures ~/;
ln -vs /home/michael/.cache/thumbnails ~/.thumbnails;
ln -vs /media/2TB_Storage/Videos ~/;
exit